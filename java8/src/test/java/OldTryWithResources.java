import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;

public class OldTryWithResources {

	@Test
	public void beforeJava9() throws IOException {
		final TemporaryFile file1 = new TemporaryFile("C:/dev/1.txt");
		final TemporaryFile file2 = new TemporaryFile("C:/dev/2.txt");

		try {
			// We need to declare new variables holding the auto closables
			try (TemporaryFile f1 = file1; TemporaryFile f2 = file2) {
				f1.doSomething();
				f2.doSomething();

				throw new IllegalArgumentException("bye bye");
			}
		} catch (IllegalArgumentException e) {
			assertFalse(new File("C:/tmp/1.txt").exists());
		}

	}


	private class TemporaryFile implements AutoCloseable {

		final File myTemporaryFileHandle;

		public TemporaryFile(final String path) throws IOException {
			myTemporaryFileHandle = new File(path);
			myTemporaryFileHandle.createNewFile();
		}

		public void doSomething() {
			System.out.println("hello");
		}

		@Override
		public void close() {
			myTemporaryFileHandle.delete();
		}
	}
}
