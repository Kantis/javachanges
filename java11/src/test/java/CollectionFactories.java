import org.junit.Test;

import java.util.*;

public class CollectionFactories {

	@Test
	public void example() {
		// Now
		var list = List.of("kalle", "jenny");

		// Before
		var list2 = new ArrayList<String>();
		list2.add("kalle");
		list2.add("jenny");
		var immutableList2 = Collections.unmodifiableList(list2);

		// Also applies to Sets
	}

	@Test
	public void iterationOrderOnSetsAndMapsIsRandom() {
		Set.of(1, 2, 3, 4, 5, 6, 7).forEach(System.out::println);
		Map.of("key1", 5, "key2", 10, "key3", 20).forEach((key, value) -> System.out.println(key + ": " + value));

		// Motivation:

		/*
		  The final safety feature is the randomized iteration order of the immutable Set elements and Map keys.
		  HashSet and HashMap iteration order has always been unspecified, but fairly stable,
		  leading to code having inadvertent dependencies on that order. This causes things to break when the
		  iteration order changes, which occasionally happens. The new Set/Map collections change their iteration
		  order from run to run, hopefully flushing out order dependencies earlier in test or development.
		 */
	}
}
