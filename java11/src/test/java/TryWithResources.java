import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;

public class TryWithResources {

	@Test
	public void example() throws IOException {

		final TemporaryFile file1 = new TemporaryFile("C:/dev/1.txt");
		final TemporaryFile file2 = new TemporaryFile("C:/dev/2.txt");

		try {
			try (file1; file2) {
				throw new IllegalArgumentException("bye bye");
			}
		} catch (IllegalArgumentException e) {
			assertFalse(new File("C:/tmp/1.txt").exists());
		}
	}

	/**
	 * {@link OldTryWithResources}
	 */
	@Ignore
	public void beforeJava9() throws IOException {
	}

	private class TemporaryFile implements AutoCloseable {

		final File myTemporaryFileHandle;

		public TemporaryFile(final String path) throws IOException {
			myTemporaryFileHandle = new File(path);
			myTemporaryFileHandle.createNewFile();
		}

		@Override
		public void close() {
			myTemporaryFileHandle.delete();
		}
	}
}
