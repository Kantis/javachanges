import lombok.Value;
import org.junit.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class StreamOfNullable {

	@Value
	private class Example {
		Integer someValue;
	}

	@Test(expected = NullPointerException.class)
	public void withoutOfNullable() {
		var nullableIntegers = List.of(new Example(5), new Example(null), new Example(10));

		// Will throw an exception since we have null values present
		nullableIntegers.stream()
						.flatMap(example -> Stream.of(example.getSomeValue()))
						.reduce(0, Integer::sum);
	}

	@Test
	public void ofNullable() {
		var nullableIntegers = List.of(new Example(5), new Example(null), new Example(10));

		// Null values are converted to empty streams
		final int sum = nullableIntegers.stream()
										.flatMap(example -> Stream.ofNullable(example.getSomeValue()))
										.reduce(0, Integer::sum);

		assertEquals(15, sum);
	}
}
