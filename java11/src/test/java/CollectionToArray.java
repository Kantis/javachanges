import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class CollectionToArray {
	@Test
	public void example() {
		List<String> names = List.of("Emil", "Rahwa", "Daniel", "Victor", "Matthias", "Albin");

		String[] nameArray = names.toArray(String[]::new);

		assertArrayEquals(new String[]{"Emil", "Rahwa", "Daniel", "Victor", "Matthias", "Albin"}, nameArray);
	}
}
