import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.regex.Pattern;

public class PatternMatchingWithRegex {

	private Pattern startsWithLowerCase = Pattern.compile("^[a-z]");


	/*
	(Since Java8)  asPredicate checks whether the **string or any substring matches** this pattern (it behaves like s -> this.matcher(s).find())
	(Since Java11) asMatchPredicate is only content if the **entire string** matches this pattern (it behaves like s -> this.matcher(s).matches())
	*/
	@Test
	public void example() throws URISyntaxException, IOException {
		var haiku = Files.readString(Path.of(getClass().getClassLoader().getResource("haiku.txt").toURI()));

		haiku.lines()
			 .filter(startsWithLowerCase.asPredicate())
			 .forEach(System.out::println);

	}
}
