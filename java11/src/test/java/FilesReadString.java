import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FilesReadString {
	@Test
	public void example() throws IOException, URISyntaxException {
		var haiku = Files.readString(Path.of(getClass().getClassLoader().getResource("haiku.txt").toURI()));

		haiku.lines()
			 .map(String::toUpperCase)
			 .forEach(System.out::println);
	}
}
