import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UnmodifiableCollections {

	@Test
	public void unmodifiableList() {
		var vegetables = new ArrayList<>(List.of("Brocolli", "Celery", "Carrot"));
		var unmodifiable = Collections.unmodifiableList(vegetables);

		vegetables.set(0, "Radish");

		assertEquals("Brocolli", unmodifiable.get(0));
	}

	@Test
	public void trueUnmodifiableList() {
		var vegetables = new ArrayList<>(List.of("Brocolli", "Celery", "Carrot"));

		// New since Java 10
		var unmodifiable = List.copyOf(vegetables);

		vegetables.set(0, "Radish");

		assertEquals("Brocolli", unmodifiable.get(0));
	}

}
