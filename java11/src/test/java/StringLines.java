import org.apache.commons.lang3.text.StrBuilder;
import org.junit.Test;

public class StringLines {

	@Test
	public void example() {

		var str = new StrBuilder().appendln("kalle")
				.appendln("jerry")
				.appendln("tom")
				.appendln("hiawatha")
				.build();

		str.lines()
		   .filter(name -> name.endsWith("e") || name.endsWith("a"))
		   .forEach(name -> System.out.println("Hello " + name));

	}

}
