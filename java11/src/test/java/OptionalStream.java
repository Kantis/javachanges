import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class OptionalStream {

	@Test
	public void streamWithOptionals() {

		var values = List.of(
				Optional.of(5),
				Optional.empty(),
				Optional.of(10),
				Optional.empty()
		);

		assertEquals(List.of(5, 10), values.stream()
										   .flatMap(Optional::stream)
										   .collect(Collectors.toList()));


		// Before Java 9 you had to filter or use a more elaborate map expression
		assertEquals(List.of(5, 10), values.stream()
										   .flatMap(opt -> opt.isPresent() ? Stream.of(opt.get()) : Stream.empty())
										   .collect(Collectors.toList()));


		assertEquals(List.of(5, 10), values.stream()
										   .filter(Optional::isPresent)
										   .map(Optional::get)
										   .collect(Collectors.toList()));


	}
}
