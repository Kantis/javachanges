import lombok.Value;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class OptionalOr {

	@Value
	public class Customer {
		String name;
	}

	public interface CustomerDao {
		Optional<Customer> findInMemory(String customerId);

		Optional<Customer> findOnDisk(String customerId);

		Optional<Customer> findRemotely(String customerId);
	}

	private CustomerDao mockDao = Mockito.mock(CustomerDao.class);

	@Test
	public void optionalOr() {
		when(mockDao.findInMemory(any())).thenReturn(Optional.empty());
		when(mockDao.findOnDisk(any())).thenReturn(Optional.empty());
		when(mockDao.findRemotely(any())).thenReturn(Optional.of(new Customer("Kalle")));

		var customerId = "1";
		Optional<Customer> customer = mockDao.findInMemory(customerId)
							  .or(() -> mockDao.findOnDisk(customerId))
							  .or(() -> mockDao.findRemotely(customerId));

		assertEquals("Kalle", customer.get().getName());
	}

	// Before java9
	@Test
	public void cantUseOptionalOr() {
		when(mockDao.findInMemory(any())).thenReturn(Optional.empty());
		when(mockDao.findOnDisk(any())).thenReturn(Optional.empty());
		when(mockDao.findRemotely(any())).thenReturn(Optional.of(new Customer("Kalle")));

		var customerId = "1";

		// Unboxes the optional..
		Customer customer = mockDao.findInMemory(customerId)
							  .orElseGet(() -> mockDao.findOnDisk(customerId)
													  .orElseGet(() -> mockDao.findRemotely(customerId).get()));

		// Forces mutability
		Optional<Customer> customer2 = mockDao.findInMemory(customerId);
		if (!customer2.isPresent()) {
			customer2 = mockDao.findOnDisk(customerId);
		}
		if (!customer2.isPresent()) {
			customer2 = mockDao.findRemotely(customerId);
		}

		assertEquals("Kalle", customer.getName());
		assertEquals("Kalle", customer2.get().getName());
	}

}
