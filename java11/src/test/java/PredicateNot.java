import org.junit.Test;

import java.util.List;
import java.util.function.Predicate;

public class PredicateNot {

	@Test
	public void not() {
		List<String> names = List.of("Emil", "Rahwa", "Daniel", "Victor", "Matthias", "Albin");

		names.stream()
			 .filter(Predicate.not(it -> it.contains("i")))
			 .forEach(System.out::println);
	}
}
