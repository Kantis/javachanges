import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringRepeat {

	@Test
	public void example() {
		assertEquals("abcabcabcabcabc", "abc".repeat(5));
	}

}
