import org.junit.Test;

import java.time.LocalDate;
import java.util.stream.Stream;

public class StreamIterate {

	@Test
	public void iterate() {
		// Creates an infinite stream with values 0 -> oo
		Stream.iterate(0, x -> x + 10)
			  .dropWhile(value -> value < 50) // Skip until we reach a value >= 50
			  .takeWhile(value -> value < 100) // Take values until we reach 100
			  .forEach(System.out::println);
	}

	@Test
	public void iterate2() {
		Stream.iterate("hej", s -> s + " hej")
			  .takeWhile(it -> it.length() < 100)
			  .forEach(System.out::println);
	}

	@Test
	public void iterateWithHasNextCondition() {

		Stream.iterate(
				LocalDate.of(2018, 1, 1),
				date -> date.isBefore(LocalDate.of(2019, 1, 1)),
				date -> date.plusDays(1)
		).forEach(System.out::println);

	}

}
